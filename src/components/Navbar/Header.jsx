import { Link } from 'react-router-dom';

export function Header() {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
      <div className='container-fluid'>
        <Link className='navbar-brand' to='/'>
          Rick and Morty
        </Link>
        <div className='collapse navbar-collapse' id='navbarText'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='/'>
                Inicio
              </Link>
            </li>
          </ul>
          {/* <span className='navbar-text'>
            <Link
              className='nav-link active'
              aria-current='page'
              to='/personaje'
            >
              Personajes
            </Link>
          </span> */}
        </div>
      </div>
    </nav>
  );
}
