import { useEffect, useState } from 'react';

import { PersonajeItem } from './PersonajeItem';
import axios from 'axios';

export function ListadoPersonajes() {
  const [personajes, setPersonajes] = useState(null);

  useEffect(() => {
    // axios permite hacer peticiones a otros servicios de una manera mas sencilla de fetch
    axios.get('https://rickandmortyapi.com/api/character').then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, []);

  return (
    <div className='row py-5'>
      {personajes
        ? personajes.results.map((elemento) => {
            return (
              <PersonajeItem
                // name={elemento.name}
                // status={elemento.status}
                // species={elemento.species}
                // location={elemento.location}
                // image={elemento.image}
                // origin={elemento.origin}
                key={elemento.id}
                {...elemento}
              />
            );
          })
        : 'Cargando...'}
    </div>
  );
}

