import { Banner } from '../components/Navbar/Banner';
import { Buscador } from '../components/Buscador/Buscador';
import { ListadoPersonajes } from '../components/Personajes/ListadoPersonajes';

export function Home() {
  return (
    <div>
      <Banner />
      <div className='px-5'>
        <h1 className='py-4'>Personajes destacados</h1>

        <Buscador />

        <ListadoPersonajes />
      </div>
    </div>
  );
}
